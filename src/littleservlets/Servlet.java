package littleservlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = "/Servlet")
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><head></head><body>");
        String answer = request.getParameter("answer");
        if (answer.equalsIgnoreCase("Banana") || answer.equalsIgnoreCase("Apple") || answer.equalsIgnoreCase("Orange")) {
            out.println("<p>Wrong, try <a href=\"/LearningServlets_war_exploded/BestFruit.html\">again</a></p>");

        } else if (answer.equalsIgnoreCase("Mango")) {
            out.println("<h1>That Right!<br>Congratulations!</h1>");
        } else {
            out.println("<p>You must enter either Banana, Apple, Mango, or Orange.<p>"+
                    "<p><a href=\"/LearningServlets_war_exploded/BestFruit.html\">Try Again</a></p>");
        }
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
